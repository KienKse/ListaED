public class Aluno {
	
	private String nome;
	private Integer idade;
	private Integer matricula = 0;

	public Aluno(String nome, Integer idade) {
		matricula += matricula++;
		this.nome = nome;
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getMatricula() {
		return matricula;
	}

}